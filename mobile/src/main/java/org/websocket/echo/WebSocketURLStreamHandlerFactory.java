package org.websocket.echo;

import java.net.URL;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;

public class WebSocketURLStreamHandlerFactory implements URLStreamHandlerFactory {

    public static void install() {
        URL.setURLStreamHandlerFactory(new WebSocketURLStreamHandlerFactory());
    }

    @Override
    public URLStreamHandler createURLStreamHandler(String protocol) {
        if (protocol == null) return null;
        if ("ws".equals(protocol))
            return new WebSocketURLStreamHandler(false);
        if ("wss".equals(protocol))
            return new WebSocketURLStreamHandler(true);
        return null;
    }

}
