package org.websocket.echo;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

public class WebSocketURLStreamHandler extends URLStreamHandler {

    final boolean isSecure;

    WebSocketURLStreamHandler(boolean secure) {
        this.isSecure = true;
    }

    @Override
    protected int getDefaultPort() {
        return isSecure ? 443 : 80;
    }

    @Override
    protected URLConnection openConnection(URL url) throws IOException {
        return new WebSocketURLConnection(url, isSecure);
    }

}
