package org.websocket.echo;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public class WebSocketParser {

    private boolean deflate;

    public void setDeflate(boolean enable) {
        this.deflate = enable;
        this.inflated = enable ? new byte[64 * 1024] : null;
    }

    //private static final int BYTE   = 0xFF;
    private static final int FIN = 0x80;
    private static final int MASK = 0x80;
    private static final int RSV1 = 0x40;
    private static final int RSV2 = 0x20;
    private static final int RSV3 = 0x10;
    private static final int OPCODE = 0x0F;
    private static final int LENGTH = 0x7F;

    private static final int OP_CONTINUATION = 0x00;
    private static final int OP_TEXT = 0x01;
    private static final int OP_BINARY = 0x02;
    private static final int OP_CLOSE = 0x08;
    private static final int OP_PING = 0x09;
    private static final int OP_PONG = 0x0A;

    static boolean isMessage(int opcode) {
        return opcode == OP_CONTINUATION || opcode == OP_TEXT || opcode == OP_BINARY;
    }

    static boolean isControl(int opcode) {
        return opcode == OP_CLOSE || opcode == OP_PING || opcode == OP_PONG;
    }

    static class Section {
        int opcode;
        boolean isLast;
        boolean isRsv1;
        boolean isRsv2;
        boolean isRsv3;
        byte[] buffer;
        int length;
    }

    protected static final byte[] TAIL = new byte[]{0x00, 0x00, (byte) 0xFF, (byte) 0xFF};

    final Inflater inflater = new Inflater(true);

    private byte[] inflated;

    private int inflate(final byte[] source, final int length) throws DataFormatException {
        inflater.setInput(source, 0, length);

        final int requested = length * 2 + 32;
        if (inflated == null || inflated.length < requested)
            inflated = new byte[requested];

        int decoded = 0;
        while (inflater.getRemaining() > 0 && !inflater.finished())
            decoded += inflater.inflate(inflated, decoded, inflated.length - decoded);
        inflater.reset();
        return decoded;
    }

    /**
     * Decoded message fragment.
     */
    final Section section = new Section();

    /**
     * Buffer to store fragmented messages content.
     */
    final ByteArrayOutputStream storage = new ByteArrayOutputStream();

    private boolean isCompressed = false;

    private WebSocketFrame finish(int opcode, int level) throws DataFormatException {
        if (section.isLast) {
            byte[] source;
            int length;
            final boolean compressed;
            if (level == 0) {
                compressed = section.isRsv1;
                if (compressed) {
                    storage.write(section.buffer, 0, section.length);
                    storage.write(TAIL, 0, TAIL.length);
                    source = storage.toByteArray();
                    length = storage.size();
                } else {
                    source = section.buffer;
                    length = section.length;
                }
            } else {
                compressed = isCompressed;
                storage.write(section.buffer, 0, section.length);
                if (compressed) storage.write(TAIL, 0, TAIL.length);
                source = storage.toByteArray();
                length = storage.size();
            }
            try {
                if (compressed) {
                    length = inflate(source, length);
                    source = inflated;
                }
                return opcode == OP_TEXT
                        ? new WebSocketFrame.Text(source, length)
                        : new WebSocketFrame.Binary(source, length);
            } finally {
                isCompressed = false;
                storage.reset();
            }
        } else {
            if (level == 0 && deflate)
                isCompressed = section.isRsv1;
            storage.write(section.buffer, 0, section.length);
            return null;
        }
    }

    private int mSaveOp, mNestNo;

    public WebSocketFrame decode(final DataInputStream stream) throws IOException {
        WebSocketFrame result;
        while (true) {
            decode(stream, mNestNo, section);
            switch (section.opcode) {
                case OP_CONTINUATION:
                case OP_TEXT:
                case OP_BINARY:
                    try {
                        result = finish(mNestNo > 0 ? mSaveOp : section.opcode, mNestNo);
                        if (result == null) {
                            // Message is not complete,
                            // wait for continuation
                            if (mNestNo == 0)  // save opcode of 1st fragment
                                mSaveOp = section.opcode;
                            mNestNo++;
                        } else {
                            // Message assembling complete
                            mNestNo = 0;
                            return result;
                        }
                    } catch (DataFormatException e) {
                        mNestNo = 0;
                    }
                    break;
                case OP_CLOSE:
                    final int status = (section.length >= 2) ? 0xFF * section.buffer[0] + section.buffer[1] : 0;
                    final String reason = (section.length > 2) ? new String(section.buffer, 2, section.length - 2) : null;
                    return new WebSocketFrame.Close(status, reason);
                case OP_PING:
                    if (section.length > 125)
                        throw new ProtocolException("Ping payload too large");
                    return new WebSocketFrame.Ping(section.buffer, section.length);
                default:  // OP_PONG
                    // discard and go on
            }
        }
    }

    private void decode(DataInputStream stream, int level, Section target) throws IOException {
        // OPCODE + flags
        int data = stream.readUnsignedByte();

        target.isRsv1 = (data & RSV1) == RSV1;
        target.isRsv2 = (data & RSV2) == RSV2;
        target.isRsv3 = (data & RSV3) == RSV3;

        final boolean isLast = target.isLast = (data & FIN) == FIN;
        final int opcode = target.opcode = (data & OPCODE);

        // MASK, LENGTH
        data = stream.readUnsignedByte();
        final boolean masked = (data & MASK) != 0;
        if (masked) {
            // currently, we don't allow this. need to see what's the final spec.
            throw new ProtocolException("masked server frame");
        }
        final int len1 = data & LENGTH;

        if (opcode > 7) {
            // control frame (have MSB in opcode set)
            // control frames MUST NOT be fragmented
            if (!isLast)
                throw new ProtocolException("fragmented control frame");

            // control frames MUST have payload 125 octets or less
            if (len1 > 125)
                throw new ProtocolException("control frame with payload length > 125 bytes");

            // check for reserved control frame opcodes
            if (!isControl(opcode))
                throw new ProtocolException("unknown control frame opcode=" + opcode);

            // close frame : if there is a body, the first two bytes of the
            // body MUST be a 2-byte unsigned integer (in network byte
            // order) representing a getStatus code
            if (opcode == OP_CLOSE && len1 == 1)
                throw new ProtocolException("received close control frame with payload len=1");
        } else {
            // message frame

            // check for reserved data frame opcodes
            if (!isMessage(opcode))
                throw new ProtocolException("unknown message frame opcode=" + opcode);

            // check opcode vs message fragmentation state 1/2
            if (level == 0 && opcode == OP_CONTINUATION)
                throw new ProtocolException("received continuation frame outside fragmented message");

            // check opcode vs message fragmentation state 2/2
            if (level > 0 && opcode != OP_CONTINUATION)
                throw new ProtocolException("received non-continuation frame while inside fragmented message");
        }

        // Actual data length
        final int length;
        switch (len1) {
            case 126:
                length = stream.readUnsignedShort();
                if (length < 126)
                    throw new ProtocolException("invalid data frame length (not using minimal length encoding)");
                break;
            case 127:
                length = (int) stream.readLong();
                if (length < 0xFFFF)
                    throw new ProtocolException("invalid data frame length (not using minimal length encoding)");
                break;
            default:
                length = len1;
        }

        // Mask (if any)
        if (masked) {
            // TODO not implemented, we`re in client only mode
        }

        // Application data
        if (length > 0) {
            if (target.length < length)
                target.buffer = new byte[length];
            target.length = length;
            stream.readFully(target.buffer, 0, length);
        } else
            target.length = 0;
    }

    final byte[] mask = new byte[4];

    private final ByteArrayOutputStream output = new ByteArrayOutputStream();

    private byte[] encode(int opcode, Object value, int error) throws IOException {
        final byte buffer[] = (value instanceof String)
                ? ((String) value).getBytes() : (byte[]) value;
        final int insert = error > 0 ? 2 : 0;  // additional bytes to insert (when error code is present)
        final int length = buffer == null ? insert : buffer.length + insert;  // total application data length

        final ByteArrayOutputStream stream = output;
        stream.reset();
        stream.write((opcode & OPCODE) | FIN);  // no fragmentation, always complete message
        // RSV1 == RSV2 == RSV3 always == 0

        // Length: I`m a client, always with MASK
        if (length <= 125)
            stream.write(MASK | length);
        else if (length < 0xFFFF) {
            stream.write(MASK | 126);
            stream.write(length >>> 8);
            stream.write(length);
        } else {
            stream.write(MASK | 127);
            stream.write(length >>> 56);
            stream.write(length >>> 48);
            stream.write(length >>> 40);
            stream.write(length >>> 32);
            stream.write(length >>> 24);
            stream.write(length >>> 16);
            stream.write(length >>> 8);
            stream.write(length);
        }

        // Masking key
        final int random = (int) (Math.random() * Integer.MAX_VALUE);
        stream.write(mask[0] = (byte) (random >>> 24));
        stream.write(mask[1] = (byte) (random >>> 16));
        stream.write(mask[2] = (byte) (random >>> 8));
        stream.write(mask[3] = (byte) (random));

        // Application data
        int count = 0;
        while (count < length) {
            // Write error code (if available),
            // then the rest application data.
            final int data = (count < insert)
                    ? (error >>> 8 * (1 - count)) & 0xFF
                    : buffer[count - insert];
            stream.write(data ^ mask[count++ % 4]);
        }

        final byte[] result = new byte[stream.size()];
        System.arraycopy(stream.toByteArray(), 0, result, 0, result.length);
        return result;
    }

    public byte[] encode(WebSocketFrame frame) throws IOException {
        if (frame instanceof WebSocketFrame.Binary)
            return encode(OP_BINARY, ((WebSocketFrame.Bytes) frame).payload, 0);
        else if (frame instanceof WebSocketFrame.Text)
            return encode(OP_TEXT, ((WebSocketFrame.Text) frame).payload, 0);
        if (frame instanceof WebSocketFrame.Close) {
            WebSocketFrame.Close close = (WebSocketFrame.Close) frame;
            return encode(OP_CLOSE, close.reason, close.status);
        }
        if (frame instanceof WebSocketFrame.Ping)
            return encode(OP_PING, ((WebSocketFrame.Bytes) frame).payload, 0);
        if (frame instanceof WebSocketFrame.Pong)
            return encode(OP_PONG, ((WebSocketFrame.Bytes) frame).payload, 0);
        else
            throw new ProtocolException("Cannot encode frame of type: " + frame.getClass().getName());
    }

}
