package org.websocket.echo;

public abstract class WebSocketFrame {

    public static class Bytes extends WebSocketFrame {

        public final byte[] payload;

        protected Bytes(byte[] buffer, int length) {
            if (length == 0 || buffer == null)
                payload = null;
            else {
                payload = new byte[length];
                System.arraycopy(buffer, 0, payload, 0, length);
            }
        }

    }

    public final static class Binary extends Bytes {

        public Binary(byte[] buffer, int length) {
            super(buffer, length);
        }

        public Binary(byte[] buffer) {
            super(buffer, buffer == null ? 0 : buffer.length);
        }

    }

    public final static class Ping extends Bytes {

        public Ping() {
            super(null, 0);
        }

        public Ping(byte[] buffer, int length) {
            super(buffer, length);
        }

        public Ping(byte[] buffer) {
            super(buffer, buffer == null ? 0 : buffer.length);
        }

    }

    public final static class Pong extends Bytes {

        public Pong(byte[] buffer) {
            super(buffer, buffer == null ? 0 : buffer.length);
        }

        public Pong(Ping ping) {
            super(ping.payload, ping.payload == null ? 0 : ping.payload.length);
        }

    }

    public final static class Text extends WebSocketFrame {

        public final String payload;

        public Text(byte[] buffer, int offset, int length) {
            if (length == 0 || buffer == null)
                payload = null;
            else
                payload = new String(buffer, offset, length);
        }

        public Text(byte[] buffer, int length) {
            this(buffer, 0, length);
        }

        public Text(byte[] buffer) {
            this(buffer, 0, buffer.length);
        }

        public Text(String text) {
            this.payload = text;
        }

    }

    public final static class Close extends WebSocketFrame {

        public final static short NORMAL = 1000;
        public final static short GONE_AWAY = 1001;
        public final static short PROTOCOL_ERROR = 1002;
        public final static short INVALID_MESSAGE = 1003;
        public final static short MESSAGE_MISMATCH = 1007;
        public final static short MESSAGE_POLICY = 1008;
        public final static short MESSAGE_TOO_BIG = 1009;
        public final static short NEED_EXTENSION = 1010;
        public final static short SERVER_FAILURE = 1011;

        public final int status;

        public final String reason;

        public Close(int code, String reason) {
            this.status = code;
            this.reason = reason;
        }

        public Close(int code) {
            this(code, null);
        }

        public Close() {
            this(1000, null);
        }

    }

}
