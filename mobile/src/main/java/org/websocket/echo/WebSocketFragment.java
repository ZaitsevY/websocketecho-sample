package org.websocket.echo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.EOFException;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A non-destroyable (survives screen orientation change) fragment
 * which controls web-socket connection.
 */
public class WebSocketFragment extends Fragment implements Handler.Callback {

    final static String ACTION_STATE = WebSocketFragment.class.getName() + ".state";

    final static String ACTION_ERROR = WebSocketFragment.class.getName() + ".error";

    final static String EXTRAS_ERROR = ACTION_ERROR;

    final static String ACTION_FRAME = WebSocketFragment.class.getName() + ".frame";

    final static String EXTRAS_FRAME = ACTION_FRAME;

    final static String TAG = WebSocketFragment.class.getName();

    static WebSocketFragment get(FragmentManager manager) {
        WebSocketFragment instance = (WebSocketFragment) manager.findFragmentByTag(TAG);
        if (instance == null) {
            instance = new WebSocketFragment();
            manager.beginTransaction().add(instance, TAG).commit();
        }
        return instance;
    }

    static {
        WebSocketURLStreamHandlerFactory.install();
    }

    private String mEndpoint;

    private Handler mWorker;

    private Handler mReader;

    private LocalBroadcastManager mBroadcast;

    public WebSocketFragment() {
        setRetainInstance(true);
        mEndpoint = "wss://echo.websocket.org";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HandlerThread thread = new HandlerThread("ws-worker");
        thread.start();
        mWorker = new Handler(thread.getLooper(), this);
        thread = new HandlerThread("ws-reader");
        thread.start();
        mReader = new Handler(thread.getLooper(), this);
        mBroadcast = LocalBroadcastManager.getInstance(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mWorker.sendEmptyMessage(MSG_CLOSE);
        mReader.getLooper().quit();
        mWorker.getLooper().quit();
    }

    public boolean isConnected() {
        return mConnected.get();
    }

    public String endpoint() {
        return mEndpoint;
    }

    public void connect(String endpoint) throws IOException {
        mEndpoint = endpoint;
        mWorker.sendEmptyMessage(MSG_START);
    }

    public void disconnect() {
        mWorker.sendEmptyMessage(MSG_CLOSE);
    }

    public void send(String message) {
       mWorker.obtainMessage(MSG_SEND, message).sendToTarget();
    }

    final static int MSG_START = 1;
    final static int MSG_CLOSE = 3;
    final static int MSG_READ = 2;
    final static int MSG_SEND = 4;

    private final AtomicBoolean mConnected = new AtomicBoolean();

    private WebSocketURLConnection mConnection;

    final void reportState() {
        final Intent intent = new Intent(ACTION_STATE);
        mBroadcast.sendBroadcast(intent);
    }

    final void reportError(final Exception error) {
        final Intent intent = new Intent(ACTION_ERROR);
        intent.putExtra(EXTRAS_ERROR, error.toString());
        mBroadcast.sendBroadcast(intent);
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MSG_START:
                // Worker thread
                if (mConnected.compareAndSet(false, true)) {
                    try {
                        final URL url = new URL(mEndpoint);
                        mConnection = (WebSocketURLConnection) url.openConnection();
                        mReader.sendEmptyMessage(MSG_READ);
                    } catch (IOException e) {
                        mConnected.set(false);
                        reportError(e);
                    }
                }
                reportState();
                break;
            case MSG_CLOSE:
                // Worker thread
                if (mConnected.compareAndSet(true, false)) {
                    mConnection.disconnect();
                    mConnection = null;
                    reportState();
                }
                break;
            case MSG_SEND:
                // Worker thread
                if (mConnected.get()) try {
                    if (msg.obj instanceof WebSocketFrame)
                        mConnection.send((WebSocketFrame) msg.obj);
                    else
                        mConnection.send(new WebSocketFrame.Text(msg.obj.toString()));
                } catch (IOException e) {
                    reportError(e);
                    mWorker.sendEmptyMessage(MSG_CLOSE);
                }
                break;
            case MSG_READ:
                // Reader thread
                try {
                    WebSocketFrame frame = mConnection.fetch();
                    Log.d(TAG, "Got " + frame.getClass().getName());
                    if (frame instanceof WebSocketFrame.Text) {
                        final Intent intent = new Intent(ACTION_FRAME);
                        intent.putExtra(EXTRAS_FRAME, ((WebSocketFrame.Text) frame).payload);
                        mBroadcast.sendBroadcast(intent);
                    }
                    if (frame instanceof WebSocketFrame.Ping) {
                        mWorker.obtainMessage(MSG_SEND,
                                new WebSocketFrame.Pong((WebSocketFrame.Ping) frame)).sendToTarget();
                    }
                    if (frame instanceof WebSocketFrame.Close) {
                        final WebSocketFrame.Close close = (WebSocketFrame.Close) frame;
                        final String reason;
                        if (close.status == WebSocketFrame.Close.NORMAL)
                            reason = "Normal close";
                        else
                            reason = TextUtils.isEmpty(close.reason) ? "Unknown reason" : close.reason;
                        reportError(new EOFException(reason));
                        mWorker.sendEmptyMessage(MSG_CLOSE);
                    } else
                        mReader.sendEmptyMessage(MSG_READ);
                } catch (IOException e) {
                    if (mConnected.get()) {
                        reportError(e);
                        mWorker.sendEmptyMessage(MSG_CLOSE);
                    }
                }
                break;
        }
        return true;
    }

}

