package org.websocket.echo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

public class MainActivity extends ActionBarActivity {

    WebSocketFragment mConnection;

    EditText mEndpoint;

    Button mConnect;

    EditText mMessage;

    Button mDeliver;

    TextView mLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mConnection = WebSocketFragment.get(getSupportFragmentManager());

        mEndpoint = (EditText) findViewById(R.id.endpoint);

        mConnect = (Button) findViewById(R.id.connect);
        mConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mConnection.isConnected())
                    mConnection.disconnect();
                else {
                    final String endpoint = mEndpoint.getText().toString();
                    // Very-very simple validation
                    if (TextUtils.isEmpty(endpoint))
                        mEndpoint.setError("Invalid connection address");
                    else try {
                        mConnection.connect(endpoint);
                    } catch (IOException e) {
                        mEndpoint.setError(e.getMessage());
                    }
                }
            }
        });

        mMessage = (EditText) findViewById(R.id.message);
        mMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    sendMessage();
                    return true;
                }
                return false;
            }
        });

        mDeliver = (Button) findViewById(R.id.deliver);
        mDeliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        mLog = (TextView) findViewById(R.id.log);

        findViewById(R.id.clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLog.setText(null);
            }
        });

        final IntentFilter filter = new IntentFilter();
        filter.addAction(WebSocketFragment.ACTION_STATE);
        filter.addAction(WebSocketFragment.ACTION_ERROR);
        filter.addAction(WebSocketFragment.ACTION_FRAME);
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mReceiver, filter);

        updateConnectionState();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    final void sendMessage() {
        final String message = mMessage.getText().toString();
        if (TextUtils.isEmpty(message)) return;
        logMessage("Sending: " + message);
        mConnection.send(message);
        mMessage.setText(null);
    }

    final void logMessage(final String text) {
        mLog.append(text);
        mLog.append("\r\n");
    }

    final void updateConnectionState() {
        final boolean connected = mConnection.isConnected();
        mConnect.setText(connected ? R.string.action_disconnect : R.string.action_connect);
        mEndpoint.setText(mConnection.endpoint());
        mEndpoint.setEnabled(!connected);
        mMessage.setEnabled(connected);
        mDeliver.setEnabled(connected);
    }

    final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (WebSocketFragment.ACTION_STATE.equals(action)) {
                updateConnectionState();
                if (mConnection.isConnected()) {
                    logMessage("Connection opened");
                    mMessage.requestFocus();
                } else
                    logMessage("Connection closed");
            }
            if (WebSocketFragment.ACTION_ERROR.equals(action)) {
                logMessage("Connection error: " + intent.getStringExtra(WebSocketFragment.EXTRAS_ERROR));
            }
            if (WebSocketFragment.ACTION_FRAME.equals(action)) {
                logMessage("Received: " + intent.getStringExtra(WebSocketFragment.EXTRAS_FRAME));
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // TODO no menu for now getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
