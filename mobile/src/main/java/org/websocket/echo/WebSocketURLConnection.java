package org.websocket.echo;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpRetryException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.SocketPermission;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Permission;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

public class WebSocketURLConnection extends URLConnection {

    final static String TAG = WebSocketURLConnection.class.getSimpleName();

    final boolean secure;

    final SocketFactory factory;

    final int port;

    WebSocketURLConnection(final URL url, final boolean secure) throws IOException {
        super(url);
        doOutput = true;

        port = (url.getPort() < 0) ? url.getDefaultPort() : url.getPort();

        this.secure = secure;
        this.factory = secure ? SSLSocketFactory.getDefault() : SocketFactory.getDefault();
    }

    private Socket socket;

    private DataInputStream istream;

    private OutputStream ostream;

    private WebSocketParser parser;

    final String createSecret() {
        final byte[] nonce = new byte[16];
        for (int i = 0; i < 16; i++)
            nonce[i] = (byte) (Math.random() * 256);
        return Base64.encodeToString(nonce, Base64.DEFAULT).trim();
    }

    final String secretAccept(final String secret) {
        // concatenate, SHA1-hash, base64-encode
        try {
            final String GUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
            final String secretGUID = secret + GUID;
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] digest = md.digest(secretGUID.getBytes());
            return Base64.encodeToString(digest, Base64.DEFAULT).trim();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    final static Set<String> reserved;

    static {
        reserved = new HashSet<String>();
        reserved.add("Upgrade");
        reserved.add("Connection");
        reserved.add("Host");
        reserved.add("Origin");
        reserved.add("Sec-WebSocket-Key");
        reserved.add("Sec-WebSocket-Version");
        reserved.add("Sec-WebSocket-Accept");
        reserved.add("Sec-WebSocket-Extensions");
    }

    private String sendHandshake(OutputStream stream) throws IOException {
        final String secret = createSecret();
        final String expect = secretAccept(secret);

        final String scheme = url.getProtocol().equals("wss") ? "https" : "http";
        String origin = "";
        try {
            origin = new URI(scheme, "//" + url.getHost(), null).toString();
        } catch (URISyntaxException x) {
            Log.wtf(TAG, x.toString());
        }

        final StringBuilder query = new StringBuilder("GET ")
                .append(TextUtils.isEmpty(url.getPath()) ? "/" : url.getPath());
        if (!TextUtils.isEmpty(url.getQuery()))
            query.append('?').append(url.getQuery());
        query.append(" HTTP/1.1\r\n")
                .append("Upgrade: websocket\r\n")
                .append("Connection: Upgrade\r\n")
                .append("Host: ").append(url.getHost()).append("\r\n")
                .append("Origin: ").append(origin).append("\r\n")
                .append("Sec-WebSocket-Key: ").append(secret).append("\r\n")
                .append("Sec-WebSocket-Version: 13\r\n");

        for (String field : requests.keySet()) {
            // Skip websocket protocol reserved headers
            if (reserved.contains(field)) continue;
            // Check for deflate-frame extension
            // http://tools.ietf.org/html/draft-tyoshino-hybi-websocket-perframe-deflate-06
            if ("Accept-Encoding".equals(field)) {
                final String value = getRequestProperty(field);
                if ("deflate".equals(value) || "gzip".equals(value))
                    query.append("Sec-WebSocket-Extensions: permessage-deflate\r\n");
            } else
                query.append(field).append(": ")
                        .append(getRequestProperty(field)).append("\r\n");
        }
        query.append("\r\n");

        final byte[] array = query.toString().getBytes();
        stream.write(array);
        stream.flush();

        return expect;
    }

    static String readLine(InputStream stream, byte[] buffer) throws IOException {
        int offset = 0;
        int backup = 0;
        while (true) {
            int letter = stream.read();
            if (letter < 0)
                throw new EOFException();
            switch (letter) {
                case '\r':
                    backup = letter;
                    break;
                case '\n':
                    if (backup == '\r')
                        return new String(buffer, 0, offset);
                    break;
                default:
                    if (offset >= buffer.length)
                        throw new IOException("Too long string");
                    buffer[offset++] = (byte) (letter & 0xFF);
            }
        }
    }

    @Override
    public void connect() throws IOException {
        synchronized (this) {
            if (connected) return;
        }

        final InetSocketAddress address = new InetSocketAddress(url.getHost(), port);
        try {
            socket = factory.createSocket();
            socket.connect(address, getConnectTimeout());
            socket.setReceiveBufferSize(16384);
            socket.setSendBufferSize(16384);
            socket.setSoTimeout(getReadTimeout());

            // TODO Only RFC 6455 at the moment.
            final InputStream sinp = socket.getInputStream();
            final OutputStream sout = socket.getOutputStream();

            final String expect = sendHandshake(sout);
            readHandshake(sinp, expect);

            // Create & configure streams
            istream = new DataInputStream(new BufferedInputStream(sinp));
            ostream = sout;

            // Create & configure parser
            parser = new WebSocketParser();
            final List<String> list = getHeaderFields().get("Sec-WebSocket-Extensions");
            if (list != null)
                parser.setDeflate(list.contains("permessage-deflate"));

            // Great, handshake is complete!
            socket.setSoTimeout(0);
            synchronized (this) {
                connected = true;
            }
        } catch (IOException e) {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException x) {
                    // swallow
                }
                socket = null;
            }
            throw e;
        }
    }

    private void readHandshake(InputStream stream, String expect) throws IOException {
        final byte[] buffer = new byte[1024];
        // Read HTTP response status
        final String status = readLine(stream, buffer);
        String[] parts = status.split(" ", 3);
        if (parts.length < 2)
            throw new ProtocolException("Invalid HTTP response: " + status);
        if ("101".equals(parts[1])) {
            // OK, got "HTTP/1.1 101 Switching protocols"
            // Now parse headers
            String header;
            while (true) {
                header = readLine(stream, buffer);
                if (TextUtils.isEmpty(header)) break;
                parts = header.split(":", 2);
                if (parts.length < 2)
                    throw new ProtocolException("Invalid HTTP header: " + header);
                final String field = parts[0].trim();
                final String[] items = parts[1].trim().split(",");
                for (String item : items) item = item.trim();
                response.put(field, Arrays.asList(items));
                Log.v(TAG, header);
            }
        } else
            throw new HttpRetryException(parts[2], Integer.parseInt(parts[1]));
        // Analyze headers
        if (!"websocket".equals(getHeaderField("Upgrade")))
            throw new HttpRetryException(
                    "Connection failed: missing Upgrade header in handshake",
                    HttpURLConnection.HTTP_INTERNAL_ERROR);
        if (!expect.equals(getHeaderField("Sec-WebSocket-Accept")))
            throw new HttpRetryException(
                    "Connection failed: invalid Sec-WebSocket-Accept",
                    HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    private final Object signal = new Object();

    public void disconnect(int code, String text) {
        synchronized (this) {
            if (!connected) return;
        }
        if (socket.isConnected()) {
            try {
                // Send out CLOSE frame
                final WebSocketFrame.Close close =
                        new WebSocketFrame.Close(code, text);
                final byte[] buffer = parser.encode(close);
                ostream.write(buffer);
                ostream.flush();
            } catch (IOException e) {
                Log.i(TAG, "Failed to send close frame: " + e.toString());
            }
            if (!secure) try {
                // Graceful socket shutdown
                // (not supported for SSLSocket)
                socket.shutdownOutput();
                // Instead of emptying socket here
                // give a reading (fetching) thread
                // a chance to empty it up to end.
                synchronized (signal) {
                    try {
                        signal.wait(500);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            } catch (IOException e) {
                Log.i(TAG, "Failed to shutdown socket: " + e.toString());
            }
        }
        try {
            socket.close();
        } catch (IOException e) {
            Log.i(TAG, "Failed to close socket: " + e.toString());
        } finally {
            socket = null;
        }
        synchronized (this) {
            connected = false;
        }
    }

    public void disconnect() {
        disconnect(WebSocketFrame.Close.NORMAL, null);
    }

    public WebSocketFrame fetch() throws IOException {
        connectIfNeeded();
        try {
            return parser.decode(istream);
        } catch (IOException e) {
            synchronized (signal) {
                // In case we're in shutdown
                signal.notify();
            }
            throw e;
        }
    }

    public void send(WebSocketFrame frame) throws IOException {
        connectIfNeeded();
        ostream.write(parser.encode(frame));
        ostream.flush();
    }

    final LinkedHashMap<String, List<String>> requests =
            new LinkedHashMap<String, List<String>>();

    final LinkedHashMap<String, List<String>> response =
            new LinkedHashMap<String, List<String>>();

    @Override
    public String getHeaderField(String name) {
        final List<String> list = response.get(name);
        if (list == null || list.isEmpty()) return null;
        return list.get(list.size() - 1);
    }

    @Override
    public Map<String, List<String>> getHeaderFields() {
        return Collections.unmodifiableMap(response);
    }

    @Override
    public String getHeaderFieldKey(int n) {
        int index = 0;
        Iterator<String> iterator = response.keySet().iterator();
        while (index < n && iterator.hasNext()) {
            iterator.next();
            index++;
        }
        return (index == n) ? iterator.next() : null;
    }

    @Override
    public String getHeaderField(int n) {
        int index = 0;
        Iterator<List<String>> iterator = response.values().iterator();
        while (index < n && iterator.hasNext()) {
            iterator.next();
            index++;
        }
        if (index == n) {
            List<String> list = iterator.next();
            if (list.size() > 0)
                return list.get(list.size() - 1);
        }
        return null;
    }

    @Override
    public Permission getPermission() throws IOException {
        return new SocketPermission(url.getHost() + ":" + port, "connect");
    }

    private void connectIfNeeded() throws IOException {
        synchronized (this) {
            if (connected) return;
        }
        connect();
    }

    @Override
    public InputStream getInputStream() throws IOException {
        connectIfNeeded();
        return istream;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        connectIfNeeded();
        return ostream;
    }

    @Override
    public void addRequestProperty(String field, String value) {
        if (connected)
            throw new IllegalStateException("Already connected");
        if (field == null)
            throw new NullPointerException("key is null");
        List<String> list = requests.get(field);
        if (list == null) {
            list = new ArrayList<String>();
            requests.put(field, list);
        }
        list.add(value);
    }

    @Override
    public String getRequestProperty(String field) {
        if (connected)
            throw new IllegalStateException("Already connected");
        List<String> list = requests.get(field);
        if (list == null || list.isEmpty()) return null;
        if (list.size() == 1)
            return list.get(0);
        else {
            String text = list.toString();
            return text.substring(1, text.length() - 1); // remove [...]
        }
    }

    @Override
    public void setRequestProperty(String field, String value) {
        if (connected)
            throw new IllegalStateException("Already connected");
        if (field == null)
            throw new NullPointerException("key is null");
        List<String> list = requests.get(field);
        if (list == null) {
            list = new ArrayList<String>();
            requests.put(field, list);
        } else
            list.clear();
        list.add(value);
    }

}
