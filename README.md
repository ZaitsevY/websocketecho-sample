# README #

Android implementation of WebSocket Echo test.
See https://www.websocket.org/echo.html.

### WebSocket Echo ###

* A simple Android implementation of WebSocket Echo example featuring persistent WebSocket connection.

![device-2014-12-14-173003.png](https://bitbucket.org/repo/K656qo/images/862922484-device-2014-12-14-173003.png)

### Contacts ###

* Yuri Zaitsev zaitsevy@gmail.com